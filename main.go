package main

import (
	"fmt"
	"net"
	"os"
	"time"

	"gitlab.com/hegjon/ecpb/internal"
)

func main() {
	args := internal.Parse()

	if args.HostPort == "" {
		fmt.Println("Missing hostname")
		os.Exit(1)
	}

	fmt.Println("Args:", args)

	_, _, err := net.SplitHostPort(args.HostPort)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	addresses := internal.ReadAddressFile(args.AddressFile)
	session := internal.SessionArgs{
		Host:      args.HostPort,
		Requests:  args.Requests / args.Concurrency,
		Addresses: addresses[0:0],
	}

	results := make(chan internal.SessionResult)

	var i uint64
	var firstStart time.Time = time.Now()
	for i = 0; i < args.Concurrency; i++ {
		go internal.Session(session, results)
	}

	var totalRequest uint64
	for i = 0; i < args.Concurrency; i++ {
		result := <-results
		if result.Err == nil {
			fmt.Print(".")
			totalRequest += result.Requests
		} else {
			fmt.Println("ERR:", result.Err)
		}
	}
	var lastEnd time.Time = time.Now()

	totalSeconds := lastEnd.Sub(firstStart).Seconds()
	totalPerSec := float64(totalRequest) / totalSeconds
	fmt.Printf("\n%d request(s) in %f seconds, %f req/s\n",
		totalRequest, totalSeconds, totalPerSec)
}
