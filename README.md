# ecpb

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/hegjon/ecpb)](https://goreportcard.com/report/gitlab.com/hegjon/ecpb)

Electrum Cash Protocol benchmarking tool

ecpb is a tool for benchmarking your Fulcrum Electrum Cash Protocol server.
It is designed to give you an impression of how your current Fulcrum
installation performs.
This especially shows you how many requests per second your Fulcrum
installation is capable of serving.

## License
This software is under [MIT license](https://en.wikipedia.org/wiki/MIT_License)
