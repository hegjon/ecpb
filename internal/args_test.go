package internal_test

import (
	"testing"

	"gitlab.com/hegjon/ecpb/internal"
)

func TestReadAddresses(t *testing.T) {
	addresses := internal.ReadAddressFile("../assets/example-addresses.txt")
	if len(addresses) != 5 {
		t.Fatal("Expected len of 5, got", len(addresses))
	}
}
