package internal

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"time"
)

type clientRequest struct {
	ID      uint64        `json:"id"`
	Version string        `json:"jsonrpc"`
	Method  string        `json:"method"`
	Params  []interface{} `json:"params"`
}

//SessionArgs contains data needed for one Session to start the benchmark
type SessionArgs struct {
	Host      string
	Requests  uint64
	Addresses []string
}

//SessionResult contains benchmark data about the session after it is done
type SessionResult struct {
	Start    time.Time
	End      time.Time
	Requests uint64
	Failures uint64
	Err      error
}

var id uint64

func call(conn io.ReadWriter, method string, params []interface{}) (string, error) {

	req := clientRequest{
		ID:      id,
		Version: "2.0",
		Method:  method,
		Params:  params,
	}
	id++
	request, err := json.Marshal(req)
	if err != nil {
		return "", err
	}

	_, err = fmt.Fprintf(conn, "%s\n", request)
	if err != nil {
		return "", err
	}

	reader := bufio.NewReader(conn)
	result, err := reader.ReadString('\n')
	if err != nil {
		return "", err
	}

	return result, nil
}

//Session reflect one client
func Session(session SessionArgs, result chan SessionResult) error {
	r := SessionResult{
		Start: time.Now(),
	}

	cancel := func(err error) error {
		r.End = time.Now()
		r.Err = err

		result <- r
		return err
	}

	conn, err := net.Dial("tcp", session.Host)
	if err != nil {
		return cancel(err)
	}

	r.Requests++
	_, err = call(conn, "server.version", []interface{}{})
	if err != nil {
		return cancel(err)
	}

	for r.Requests < session.Requests {
		address := "bitcoincash:qp3wjpa3tjlj042z2wv7hahsldgwhwy0rq9sywjpyy"

		_, err = call(conn, "blockchain.address.get_balance", []interface{}{address})
		if err != nil {
			return cancel(err)
		}

		_, err = call(conn, "blockchain.address.listunspent", []interface{}{address})
		if err != nil {
			return cancel(err)
		}

		_, err = call(conn, "blockchain.address.get_history", []interface{}{address})
		if err != nil {
			return cancel(err)
		}

		r.Requests += 3
	}

	r.End = time.Now()
	result <- r
	return nil
}
