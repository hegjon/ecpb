package internal

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/pflag"
)

//Args contains all the command line arguments
type Args struct {
	Concurrency uint64
	Requests    uint64
	AddressFile string
	HostPort    string
}

// ReadAddressFile returns an array of strings
// from a file where the addresses are seperated by new line
func ReadAddressFile(name string) []string {
	file, err := os.Open(name)
	if err != nil {
		fmt.Println("Error:", err)
		os.Exit(-2)
	}
	defer file.Close()

	var addresses []string
	reader := bufio.NewReader(file)
	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			break
		}
		trimmed := strings.TrimSpace(line)
		if len(trimmed) > 0 {
			addresses = append(addresses, trimmed)
		}
	}

	return addresses
}

//Parse will parse the command line args
func Parse() Args {
	concurrency := pflag.Uint64P("concurrency", "c", 1, "Number of multiple requests to perform at a time. Default is one request at a time.")
	requests := pflag.Uint64P("requests", "n", 1, "Number of total requests to perform for the benchmarking session. The default is to just perform a single request which usually leads to non-representative benchmarking results.")
	address := pflag.StringP("address", "a", "", "File that contains all the bitcoin cash addresses to use. One address per line. Default is 'bitcoincash:qp3wjpa3tjlj042z2wv7hahsldgwhwy0rq9sywjpyy' as its content.")
	pflag.Parse()

	hostPort := pflag.Arg(0)

	return Args{
		Concurrency: *concurrency,
		Requests:    *requests,
		AddressFile: *address,
		HostPort:    hostPort,
	}
}
